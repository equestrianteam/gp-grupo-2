package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JTextPane;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

import javax.swing.JCheckBox;

import DAO.FuncionarioDAO;
import Model.ModeloTabelaCavalo;
import Model.ModeloTabelaFuncionario;

import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JDateChooser;
import javax.swing.JScrollPane;

public class CadastrarFuncionario extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtCpf;
	private JTextField txtTel;
	private JTextField txtMail;
	private JTextField txtDpt;
	private JTextField txtDias;
	private JDateChooser Data;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastrarFuncionario frame = new CadastrarFuncionario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CadastrarFuncionario() {
		setTitle("Cadastrar Funcion\u00E1rio");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 471, 536);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCadastrarFuncionrios = new JLabel("Cadastrar Funcion\u00E1rios");
		lblCadastrarFuncionrios.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblCadastrarFuncionrios.setBounds(135, 11, 190, 14);
		contentPane.add(lblCadastrarFuncionrios);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(10, 49, 97, 14);
		contentPane.add(lblNome);
		
		JLabel lblCpf = new JLabel("CPF:");
		lblCpf.setBounds(10, 85, 79, 14);
		contentPane.add(lblCpf);
		
		JLabel lblNewLabel = new JLabel("Nascimento:");
		lblNewLabel.setBounds(10, 122, 97, 14);
		contentPane.add(lblNewLabel);
		
		txtNome = new JTextField();
		txtNome.setBounds(123, 46, 322, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		txtCpf = new JTextField();
		txtCpf.setBounds(123, 82, 322, 20);
		contentPane.add(txtCpf);
		txtCpf.setColumns(10);
		
		JLabel lblTelefone = new JLabel("Telefone:");
		lblTelefone.setBounds(10, 158, 79, 14);
		contentPane.add(lblTelefone);
		
		JLabel lblEmail = new JLabel("E-Mail:");
		lblEmail.setBounds(10, 192, 66, 14);
		contentPane.add(lblEmail);
		
		txtTel = new JTextField();
		txtTel.setBounds(123, 155, 322, 20);
		contentPane.add(txtTel);
		txtTel.setColumns(10);
		
		txtMail = new JTextField();
		txtMail.setBounds(123, 189, 322, 20);
		contentPane.add(txtMail);
		txtMail.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Dias de Trabalho:");
		lblNewLabel_1.setBounds(10, 231, 141, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblHorrio = new JLabel("Hor\u00E1rio:");
		lblHorrio.setBounds(10, 272, 97, 14);
		contentPane.add(lblHorrio);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(123, 272, 319, 108);
		contentPane.add(scrollPane);
		
		JTextPane txtHora = new JTextPane();
		scrollPane.setViewportView(txtHora);
		txtHora.setForeground(Color.BLACK);
		txtHora.setBackground(Color.WHITE);
		
		JLabel lblDepartamento = new JLabel("Departamento:");
		lblDepartamento.setBounds(10, 411, 124, 14);
		contentPane.add(lblDepartamento);
		
		txtDpt = new JTextField();
		txtDpt.setBounds(123, 408, 322, 20);
		contentPane.add(txtDpt);
		txtDpt.setColumns(10);
		
		JButton btnFechar = new JButton("Fechar");
		btnFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				dispose();
				
			}
		});
		btnFechar.setBounds(10, 448, 97, 45);
		contentPane.add(btnFechar);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
				
				
				String nome = txtNome.getText();
				String cpf = txtCpf.getText();
				String nasc = sdf.format(Data.getDate());
				String tel = txtTel.getText();
				String mail = txtMail.getText();
				String dpt = txtDpt.getText();
				String dias = txtDias.getText();
				String hora = txtHora.getText();
				
				txtNome.setText("");
				txtCpf.setText("");
				txtTel.setText("");
				txtMail.setText("");
				txtDpt.setText("");
				txtDias.setText("");
				txtHora.setText("");
			
				
				
				
				FuncionarioDAO dao = new FuncionarioDAO();
				
				ModeloTabelaFuncionario funcionario = new ModeloTabelaFuncionario(nome, cpf, nasc , tel, mail, dias, hora,dpt);
				
				
				if(dao.Inserir(funcionario)){
					JOptionPane.showMessageDialog(null, "Dados inseridos no banco !");
				} else {
					JOptionPane.showMessageDialog(null, "Erro ao gravar no banco !");
				}
				
				
				

			}
		});
		btnCadastrar.setBounds(348, 448, 97, 45);
		contentPane.add(btnCadastrar);
		
		txtDias = new JTextField();
		txtDias.setBounds(123, 228, 322, 20);
		contentPane.add(txtDias);
		txtDias.setColumns(10);
		
		Data = new JDateChooser();
		Data.setBounds(123, 116, 322, 20);
		contentPane.add(Data);
	}
}
