package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import Controller.ControllerAluno;
import Controller.ControllerCavalo;
import Model.ModeloTabelaAluno;
import Model.ModeloTabelaCavalo;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.awt.Toolkit;

public class ListarAlunos extends JFrame {

	private JPanel contentPane;
	private JTextField txtPesquisa;
	private JTable table;
	ControllerAluno controle = new ControllerAluno();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ListarAlunos frame = new ListarAlunos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ListarAlunos() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(ListarAlunos.class.getResource("/Imgs/listar.jpg")));
		setTitle("Listrar Alunos");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 731, 389);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDigiteOCpf = new JLabel("Digite o CPF do aluno:");
		lblDigiteOCpf.setBounds(10, 11, 135, 14);
		contentPane.add(lblDigiteOCpf);
		
		txtPesquisa = new JTextField();
		txtPesquisa.setColumns(10);
		txtPesquisa.setBounds(155, 8, 405, 20);
		contentPane.add(txtPesquisa);
		
		JButton button = new JButton("Pesquisar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				 PesquisarALunos();
				
				
			}
		});
		button.setBounds(570, 7, 135, 23);
		contentPane.add(button);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 47, 695, 224);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Nome", "CPF", "Nascimento", "Telefone", "E-Mail", "Aulas", "Horas", "Equipamentos?", "Cavalo?", "Pulo"
			}
		));
		scrollPane.setViewportView(table);
		
		JButton button_1 = new JButton("Fechar");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				dispose();
				
			}
		});
		button_1.setBounds(10, 298, 135, 41);
		contentPane.add(button_1);
		
		JButton button_2 = new JButton("Listar Todos");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				listarALunos();
				
			}
		});
		button_2.setBounds(570, 298, 135, 41);
		contentPane.add(button_2);
	}
	
	private void listarALunos() {
		

		ArrayList<ModeloTabelaAluno> aluno = controle.getAluno();
		if (aluno != null) {
			DefaultTableModel model = modelTable();
			for (int i = 0; i < aluno.size(); i++) {
				String nome = aluno.get(i).getNome();
				String aula = aluno.get(i).getAulas();
				String nasc = aluno.get(i).getNascimento();
				String cpf = aluno.get(i).getCpf();
				String cavalo = aluno.get(i).getPossuiCavalo();
				String email = aluno.get(i).getEmail();
				String equip = aluno.get(i).getEquipamentos();
				String pulo = aluno.get(i).getPulo();
				String tel = aluno.get(i).getTelefone();
				String hora = aluno.get(i).getHorarios();


				Object[] row = {nome, cpf, email, tel, nasc, aula, hora, equip, cavalo, pulo   };
				
				

				model.addRow(row);
			}
			table.setModel(model);

		} else {
			JOptionPane.showMessageDialog(null, "N�o h� livros para listar");
		}
	}

	//Metodo para incializa��o da formata��o da tabela para pesquisa
	private DefaultTableModel modelTable() {
		DefaultTableModel model = new DefaultTableModel(new Object[][] {

		}, new String[] { "Nome", "CPF", "E-Mail", "Telefone", "Nascimento", "Aulas", "Horas","Equipamento?", "Cavalo?", "Pulo?" }) {//campos da tabela
			private static final long serialVersionUID = 1L;
			Class[] types = new Class[] { String.class, int.class, String.class, String.class,String.class, String.class, String.class, String.class, String.class, String.class };
			boolean[] canEdit = new boolean[] { false, false, false, false, false, false, false, false, false, false };

			public Class getColumnClass(int columnIndex) {
				return types[columnIndex];
			}

			public boolean isCellEditable(int rowIndex, int columnIndex) {
				return canEdit[columnIndex];
			}
		};
		return model;
	}
	
	private void PesquisarALunos() {
		
		String codigo = txtPesquisa.getText();
		
		ArrayList<ModeloTabelaAluno> aluno = controle.getPesquisar(codigo);
		if (aluno != null) {
			DefaultTableModel model = modelTable();
			for (int i = 0; i < aluno.size(); i++) {
				String nome = aluno.get(i).getNome();
				String aula = aluno.get(i).getAulas();
				String nasc = aluno.get(i).getNascimento();
				String cpf = aluno.get(i).getCpf();
				String cavalo = aluno.get(i).getPossuiCavalo();
				String email = aluno.get(i).getEmail();
				String equip = aluno.get(i).getEquipamentos();
				String pulo = aluno.get(i).getPulo();
				String tel = aluno.get(i).getTelefone();
				String hora = aluno.get(i).getHorarios();


				Object[] row = {nome, cpf, email, tel, nasc, aula, hora, equip, cavalo, pulo   };
				
				

				model.addRow(row);
			}
			table.setModel(model);

		} else {
			JOptionPane.showMessageDialog(null, "N�o alunos para listar");
		}
		
		
		
		
	}
}
