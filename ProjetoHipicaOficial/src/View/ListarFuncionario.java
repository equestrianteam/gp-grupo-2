package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.Toolkit;

import javax.swing.JTable;
import javax.swing.JScrollPane;

import Controller.ControllerCavalo;
import Controller.ControllerFuncionario;
import Model.ModeloTabelaCavalo;
import Model.ModeloTabelaFuncionario;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class ListarFuncionario extends JFrame {

	private JPanel contentPane;
	private JTextField txtPesquisaFuncionario;
	private JTable table;
	ControllerFuncionario controle = new ControllerFuncionario();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ListarFuncionario frame = new ListarFuncionario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ListarFuncionario() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(ListarFuncionario.class.getResource("/Imgs/listar.jpg")));
		setTitle("LIstar Funcionario");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 660, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Digite o c\u00F3digo do cavalo:");
		label.setBounds(10, 21, 135, 14);
		contentPane.add(label);
		
		txtPesquisaFuncionario = new JTextField();
		txtPesquisaFuncionario.setColumns(10);
		txtPesquisaFuncionario.setBounds(155, 18, 271, 20);
		contentPane.add(txtPesquisaFuncionario);
		
		JButton button = new JButton("Pesquisar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		button.setBounds(545, 17, 89, 23);
		contentPane.add(button);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 46, 624, 250);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JButton button_1 = new JButton("Fechar");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				dispose();
				
			}
		});
		button_1.setBounds(10, 310, 135, 41);
		contentPane.add(button_1);
		
		JButton button_2 = new JButton("Listar Todos");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				listarFuncionario();
				
			}
		});
		button_2.setBounds(499, 310, 135, 41);
		contentPane.add(button_2);
	}
	
	
	private void listarFuncionario() {
		String codigo = txtPesquisaFuncionario.getText();

		ArrayList<ModeloTabelaFuncionario> funcionario = controle.getFuncionario();
		if (funcionario != null) {
			DefaultTableModel model = modelTable();
			for (int i = 0; i < funcionario.size(); i++) {
				
				
				String nome = funcionario.get(i).getNome();
				String cpf = funcionario.get(i).getCpf();
				String nasc = funcionario.get(i).getNascimento();
				String tel = funcionario.get(i).getTelefone();
				String mail = funcionario.get(i).getEmail();
				String dpt = funcionario.get(i).getDepartamento();
				String dias = funcionario.get(i).getDiasDeTrabalho();
				String hora = funcionario.get(i).getHorario();
				
				


				Object[] row = { nome, cpf, nasc, tel, mail, dpt, dias, hora};

				model.addRow(row);
			}
			table.setModel(model);

		} else {
			JOptionPane.showMessageDialog(null, "N�o h� funcionarios para listar");
		}
	}

	//Metodo para incializa��o da formata��o da tabela para pesquisa
	private DefaultTableModel modelTable() {
		DefaultTableModel model = new DefaultTableModel(new Object[][] {

		}, new String[] { "Nome", "CPF", "Nascimento", "Telefone", "Email", "Departamento", "Dias de Trabalho","Horario" }) {//campos da tabela
			private static final long serialVersionUID = 1L;
			Class[] types = new Class[] { String.class, int.class, String.class, String.class,String.class, String.class, String.class, String.class };
			boolean[] canEdit = new boolean[] { false, false, false, false, false, false, false, false };

			public Class getColumnClass(int columnIndex) {
				return types[columnIndex];
			}

			public boolean isCellEditable(int rowIndex, int columnIndex) {
				return canEdit[columnIndex];
			}
		};
		return model;
		
	}


	{
	}
}
