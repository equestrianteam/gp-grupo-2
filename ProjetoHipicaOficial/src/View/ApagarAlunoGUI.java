package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JTextField;

import Controller.ControllerAluno;
import Controller.ControllerFuncionario;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;

public class ApagarAlunoGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtCpf;
	ControllerAluno controle = new ControllerAluno();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ApagarAlunoGUI frame = new ApagarAlunoGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ApagarAlunoGUI() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(ApagarAlunoGUI.class.getResource("/Imgs/010_trash-2-128.png")));
		setTitle("Apagar Aluno");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 412, 218);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Apagar Aluno");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel.setBounds(129, 11, 229, 31);
		contentPane.add(lblNewLabel);
		
		JLabel lblCpfDoAluno = new JLabel("Cpf do Aluno:");
		lblCpfDoAluno.setBounds(21, 72, 159, 14);
		contentPane.add(lblCpfDoAluno);
		
		JButton button = new JButton("Fechar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				dispose();
				
			}
		});
		button.setBounds(21, 116, 98, 46);
		contentPane.add(button);
		
		JButton btnApagar = new JButton("Apagar");
		btnApagar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				DeletarAluno();
				

				
			}
		});
		btnApagar.setBounds(273, 116, 98, 46);
		contentPane.add(btnApagar);
		
		txtCpf = new JTextField();
		txtCpf.setBounds(105, 69, 266, 20);
		contentPane.add(txtCpf);
		txtCpf.setColumns(10);
		
		JButton btnApagarTudo = new JButton("Apagar tudo");
		btnApagarTudo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				DeletarAlunoTudo();
				
			}
		});
		btnApagarTudo.setBounds(129, 128, 134, 23);
		contentPane.add(btnApagarTudo);
	}
	
	private void DeletarAluno(){
		
		String cpf = txtCpf.getText();
		
		//Posso deletar por chassi e por placa
		if(cpf.equals("")){
			return;
		}
		
		controle.DeletarAluno(cpf);
		
			
	}
	
private void DeletarAlunoTudo(){
		
		
		int cod = JOptionPane.showConfirmDialog(null, "Tem certeza?");
		
		switch(cod){
			
			case 0: controle.DeletarTudoAluno();
			
			case 1: return;
			
		}
		
	
			
	}

	
}
