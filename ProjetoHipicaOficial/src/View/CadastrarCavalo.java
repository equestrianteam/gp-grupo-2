package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import Controller.ControllerCavalo;
import DAO.AlunoDAO;
import DAO.CavaloDAO;
import Model.ModeloTabelaAluno;
import Model.ModeloTabelaCavalo;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.util.Random;

import com.toedter.calendar.JDateChooser;

import javax.swing.ImageIcon;

import java.awt.Toolkit;

public class CadastrarCavalo extends JFrame {

	private JPanel contentPane;
	private JTextField txtProp;
	private JTextField txtNome;
	private JTextField txtPelagem;
	private JTextField txtRaca;
	private JTextField txtNum;
	private JTextField txtAlim;
	private JTextField txtTrat;
	private JDateChooser Data;
	private JTextField txtCod;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastrarCavalo frame = new CadastrarCavalo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CadastrarCavalo() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(CadastrarCavalo.class.getResource("/Imgs/farm_agriculture-10-128.png")));
		setTitle("Cadastrar Cavalo");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 486, 583);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCadastrarAluno = new JLabel("Cadastrar Cavalo");
		lblCadastrarAluno.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblCadastrarAluno.setBounds(164, 11, 194, 14);
		contentPane.add(lblCadastrarAluno);
		
		JLabel lblProprietrio = new JLabel("Propriet\u00E1rio:");
		lblProprietrio.setBounds(10, 55, 78, 14);
		contentPane.add(lblProprietrio);
		
		txtProp = new JTextField();
		txtProp.setBounds(113, 52, 314, 20);
		contentPane.add(txtProp);
		txtProp.setColumns(10);
		
		JLabel lblNome = new JLabel("Nome: ");
		lblNome.setBounds(10, 97, 59, 14);
		contentPane.add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setBounds(113, 94, 314, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblPelagem = new JLabel("Pelagem:");
		lblPelagem.setBounds(10, 141, 59, 14);
		contentPane.add(lblPelagem);
		
		txtPelagem = new JTextField();
		txtPelagem.setBounds(113, 138, 314, 20);
		contentPane.add(txtPelagem);
		txtPelagem.setColumns(10);
		
		JLabel lblNascimento = new JLabel("Nascimento:");
		lblNascimento.setBounds(10, 183, 78, 14);
		contentPane.add(lblNascimento);
		
		JLabel lblRaa = new JLabel("Ra\u00E7a:");
		lblRaa.setBounds(10, 226, 46, 14);
		contentPane.add(lblRaa);
		
		txtRaca = new JTextField();
		txtRaca.setBounds(113, 223, 314, 20);
		contentPane.add(txtRaca);
		txtRaca.setColumns(10);
		
		JLabel lblNmeroDeBaia = new JLabel("N\u00FAmero de Baia:");
		lblNmeroDeBaia.setBounds(10, 271, 116, 14);
		contentPane.add(lblNmeroDeBaia);
		
		txtNum = new JTextField();
		txtNum.setBounds(113, 268, 314, 20);
		contentPane.add(txtNum);
		txtNum.setColumns(10);
		
		JLabel lblAlimentao = new JLabel("Alimenta\u00E7\u00E3o:");
		lblAlimentao.setBounds(10, 312, 99, 14);
		contentPane.add(lblAlimentao);
		
		txtAlim = new JTextField();
		txtAlim.setBounds(113, 309, 314, 20);
		contentPane.add(txtAlim);
		txtAlim.setColumns(10);
		
		JButton btnFechar = new JButton("Fechar");
		btnFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				dispose();
				
			}
		});
		btnFechar.setBounds(10, 470, 187, 51);
		contentPane.add(btnFechar);
		
		JButton btnVeterinrio = new JButton("Veterin\u00E1rio");
		btnVeterinrio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				VeterinarioGUI a = new VeterinarioGUI();
				a.setVisible(true);
				
				
			}
		});
		btnVeterinrio.setBounds(10, 429, 186, 30);
		contentPane.add(btnVeterinrio);
		
		JButton btnNewButton = new JButton("Ferreiro");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				FerreiroGUI a = new FerreiroGUI();
				a.setVisible(true);
				
			}
		});
		btnNewButton.setBounds(241, 429, 186, 30);
		contentPane.add(btnNewButton);
		
		JLabel lblTratador = new JLabel("Tratador:");
		lblTratador.setBounds(10, 387, 78, 14);
		contentPane.add(lblTratador);
		
		txtTrat = new JTextField();
		txtTrat.setBounds(113, 384, 314, 20);
		contentPane.add(txtTrat);
		txtTrat.setColumns(10);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				InserirCavalo();
			}
		});
		btnCadastrar.setBounds(241, 470, 186, 51);
		contentPane.add(btnCadastrar);
		
		Data = new JDateChooser();
		Data.setBounds(112, 183, 314, 20);
		contentPane.add(Data);
		
		JLabel lblNewLabel = new JLabel("Codigo");
		lblNewLabel.setBounds(10, 348, 46, 14);
		contentPane.add(lblNewLabel);
		
		txtCod = new JTextField();
		txtCod.setBounds(113, 340, 314, 20);
		contentPane.add(txtCod);
		txtCod.setColumns(10);
	}
	
	private void InserirCavalo() {
		
		ControllerCavalo controle= new ControllerCavalo();
		
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

		String Nome = txtNome.getText();
		String prop = txtProp.getText();		
		String nasc = sdf.format(Data.getDate());
		String raca = txtRaca.getText();
		String num = txtNum.getText();
		String alim = txtAlim.getText();
		String trat = txtTrat.getText();
		String pelo = txtPelagem.getText();
		int cod = Integer.parseInt(txtCod.getText());
		
		

		controle.Inserir(Nome, prop, nasc, raca, num, alim, trat, pelo, cod);
}

}
