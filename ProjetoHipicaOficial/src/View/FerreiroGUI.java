package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.time.DayOfWeek;
import java.util.Date;

import DAO.FerreiroDAO;
import Model.ModeloTabelaFerreiro;

import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JDayChooser;
import com.toedter.calendar.JYearChooser;
import com.toedter.calendar.JMonthChooser;

import javax.swing.border.LineBorder;

import java.awt.Color;
import java.awt.SystemColor;

import javax.swing.border.EtchedBorder;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import com.toedter.calendar.JDateChooser;
import javax.swing.ImageIcon;

public class FerreiroGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtProp;
	private JTextField txtValor;
	private JTextField txtTam;
	private JTextField txtCavalo;
	private JDateChooser data;
	private JDateChooser date;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FerreiroGUI frame = new FerreiroGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FerreiroGUI() {
		setTitle("Ferreiro");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 459, 360);
		contentPane = new JPanel();
		contentPane.setBorder(new LineBorder(new Color(180, 180, 180), 1, true));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblFerreiro = new JLabel("Ferreiro");
		lblFerreiro.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblFerreiro.setBounds(163, 11, 186, 14);
		contentPane.add(lblFerreiro);
		
		JLabel lblNomeDoCavalo = new JLabel("Nome do Cavalo:");
		lblNomeDoCavalo.setBounds(10, 58, 127, 14);
		contentPane.add(lblNomeDoCavalo);
		
		txtCavalo = new JTextField();
		txtCavalo.setBounds(147, 55, 280, 20);
		contentPane.add(txtCavalo);
		txtCavalo.setColumns(10);
		
		JLabel lblProprietrioDoCavalo = new JLabel("Propriet\u00E1rio do Cavalo:");
		lblProprietrioDoCavalo.setBounds(10, 104, 139, 14);
		contentPane.add(lblProprietrioDoCavalo);
		
		txtProp = new JTextField();
		txtProp.setBounds(147, 101, 280, 20);
		contentPane.add(txtProp);
		txtProp.setColumns(10);
		
		JLabel lblDataDaConsulta = new JLabel("Data da Consulta:");
		lblDataDaConsulta.setBounds(10, 144, 111, 14);
		contentPane.add(lblDataDaConsulta);
		
		JLabel lblValor = new JLabel("Valor:");
		lblValor.setBounds(10, 191, 46, 14);
		contentPane.add(lblValor);
		
		txtValor = new JTextField();
		txtValor.setBounds(147, 188, 280, 20);
		contentPane.add(txtValor);
		txtValor.setColumns(10);
		
		JLabel lblTamanhoDaFerradura = new JLabel("Tamanho da Ferradura:");
		lblTamanhoDaFerradura.setBounds(10, 232, 157, 14);
		contentPane.add(lblTamanhoDaFerradura);
		
		txtTam = new JTextField();
		txtTam.setBounds(147, 229, 280, 20);
		contentPane.add(txtTam);
		txtTam.setColumns(10);
		
		JButton button = new JButton("Fechar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				dispose();
				
			}
		});
		button.setBounds(10, 266, 98, 46);
		contentPane.add(button);
		
		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy/MM/dd");
				
			String cavalo = txtCavalo.getText();
			String prop = txtProp.getText();
			String dataa = sdf.format(date.getDate());
			double valor = Double.parseDouble(txtValor.getText());	
			double tamanho = Double.parseDouble(txtTam.getText());	
			
			ModeloTabelaFerreiro ferreiro = new ModeloTabelaFerreiro(cavalo, prop, dataa, valor, tamanho);
			
			FerreiroDAO dao = new FerreiroDAO();
			
			if(dao.Inserir(ferreiro)){
				JOptionPane.showMessageDialog(null, "Dados inseridos no banco !");
			} else {
				JOptionPane.showMessageDialog(null, "Erro ao gravar no banco !");
			}
			
			

				
			}
		});
		btnSalvar.setBounds(329, 266, 98, 46);
		contentPane.add(btnSalvar);
		
		
		date = new JDateChooser();
		date.getCalendarButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		date.setBounds(147, 144, 280, 20);
		contentPane.add(date);
	}
}
