package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.Toolkit;

public class MainFrame extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(MainFrame.class.getResource("/Imgs/17-128.png")));
		setResizable(false);
		setTitle("H\u00CDPICA ODARP");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 487, 341);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnCadastrar = new JMenu("Cadastrar");
		mnCadastrar.setIcon(new ImageIcon(MainFrame.class.getResource("/Imgs/save.png")));
		mnCadastrar.setFont(new Font("Segoe UI Semibold", Font.BOLD, 15));
		menuBar.add(mnCadastrar);
		
		JMenuItem mntmAluno = new JMenuItem("Aluno");
		mntmAluno.setIcon(new ImageIcon(MainFrame.class.getResource("/Imgs/aluno.png")));
		mntmAluno.setFont(new Font("Segoe UI Semibold", Font.BOLD, 15));
		mntmAluno.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				CadastrarAluno a = new CadastrarAluno();
				a.setVisible(true);
				
			}
		});
		mnCadastrar.add(mntmAluno);
		
		JMenuItem mntmCavalo = new JMenuItem("Cavalo");
		mntmCavalo.setIcon(new ImageIcon(MainFrame.class.getResource("/Imgs/farm_agriculture-10-24.png")));
		mntmCavalo.setFont(new Font("Segoe UI Semibold", Font.BOLD, 15));
		mntmCavalo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				CadastrarCavalo a = new CadastrarCavalo();
				a.setVisible(true);
				
			}
		});
		mnCadastrar.add(mntmCavalo);
		
		JMenuItem mntmFuncionrio = new JMenuItem("Funcion\u00E1rio");
		mntmFuncionrio.setIcon(new ImageIcon(MainFrame.class.getResource("/Imgs/Construction_25-18-24.png")));
		mntmFuncionrio.setFont(new Font("Segoe UI Semibold", Font.BOLD, 15));
		mntmFuncionrio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				CadastrarFuncionario a = new CadastrarFuncionario();
				a.setVisible(true);
				
				
				
			}
		});
		mnCadastrar.add(mntmFuncionrio);
		
		JMenu mnAlterar = new JMenu("Alterar");
		mnAlterar.setIcon(new ImageIcon(MainFrame.class.getResource("/Imgs/compose-24.png")));
		mnAlterar.setFont(new Font("Segoe UI Semibold", Font.BOLD, 15));
		menuBar.add(mnAlterar);
		
		JMenuItem mntmAluno_1 = new JMenuItem("Aluno");
		mntmAluno_1.setIcon(new ImageIcon(MainFrame.class.getResource("/Imgs/aluno.png")));
		mntmAluno_1.setFont(new Font("Segoe UI Semibold", Font.BOLD, 15));
		mntmAluno_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				AlterarAluno a = new AlterarAluno();
				a.setVisible(true);
	
			}
		
		});
		mnAlterar.add(mntmAluno_1);
		
		JMenuItem mntmCavalo_1 = new JMenuItem("Cavalo");
		mntmCavalo_1.setIcon(new ImageIcon(MainFrame.class.getResource("/Imgs/farm_agriculture-10-24.png")));
		mntmCavalo_1.setFont(new Font("Segoe UI Semibold", Font.BOLD, 15));
		mntmCavalo_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				

				AlterarCavalo a = new AlterarCavalo();
				a.setVisible(true);
	
				
			}
		});
		mnAlterar.add(mntmCavalo_1);
		
		JMenuItem mntmFuncionrio_1 = new JMenuItem("Funcion\u00E1rio");
		mntmFuncionrio_1.setIcon(new ImageIcon(MainFrame.class.getResource("/Imgs/Construction_25-18-24.png")));
		mntmFuncionrio_1.setFont(new Font("Segoe UI Semibold", Font.BOLD, 15));
		mnAlterar.add(mntmFuncionrio_1);
		
		JMenu mnPesquisarPor = new JMenu("Pesquisar por:");
		mnPesquisarPor.setIcon(new ImageIcon(MainFrame.class.getResource("/Imgs/09_search-24.png")));
		mnPesquisarPor.setFont(new Font("Segoe UI Semibold", Font.BOLD, 15));
		menuBar.add(mnPesquisarPor);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Aluno");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				ListarAlunos a = new ListarAlunos();
				a.setVisible(true);
				
			}
		});
		mntmNewMenuItem.setIcon(new ImageIcon(MainFrame.class.getResource("/Imgs/aluno.png")));
		mntmNewMenuItem.setFont(new Font("Segoe UI Semibold", Font.BOLD, 15));
		mnPesquisarPor.add(mntmNewMenuItem);
		
		JMenuItem mntmCavalo_2 = new JMenuItem("Cavalo");
		mntmCavalo_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				ListarCavalo a = new ListarCavalo();
				a.setVisible(true);
				
				
			}
		});
		mntmCavalo_2.setIcon(new ImageIcon(MainFrame.class.getResource("/Imgs/farm_agriculture-10-24.png")));
		mntmCavalo_2.setFont(new Font("Segoe UI Semibold", Font.BOLD, 15));
		mnPesquisarPor.add(mntmCavalo_2);
		
		JMenuItem mntmFuncionrio_2 = new JMenuItem("Funcion\u00E1rio");
		mntmFuncionrio_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				ListarFuncionario a = new ListarFuncionario();
				a.setVisible(true);
				
			}
		});
		mntmFuncionrio_2.setIcon(new ImageIcon(MainFrame.class.getResource("/Imgs/Construction_25-18-24.png")));
		mntmFuncionrio_2.setFont(new Font("Segoe UI Semibold", Font.BOLD, 15));
		mnPesquisarPor.add(mntmFuncionrio_2);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(MainFrame.class.getResource("/Imgs/Logo.png")));
		lblNewLabel.setBounds(0, 0, 481, 286);
		contentPane.add(lblNewLabel);
	}

}
