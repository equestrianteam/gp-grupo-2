package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JTextField;

import Controller.ControllerAluno;
import Controller.ControllerCavalo;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;

public class ApagarCavaloGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtCod;
	ControllerCavalo controle = new ControllerCavalo();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ApagarCavaloGUI frame = new ApagarCavaloGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ApagarCavaloGUI() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(ApagarCavaloGUI.class.getResource("/Imgs/010_trash-2-128.png")));
		setTitle("Apagar Cavalo");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 412, 218);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Apagar Cavalo");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel.setBounds(129, 11, 229, 31);
		contentPane.add(lblNewLabel);
		
		JLabel lblCpfDoAluno = new JLabel("C\u00F3digo Cavalo:");
		lblCpfDoAluno.setBounds(21, 72, 159, 14);
		contentPane.add(lblCpfDoAluno);
		
		JButton button = new JButton("Fechar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				dispose();
				
			}
		});
		button.setBounds(21, 116, 98, 46);
		contentPane.add(button);
		
		JButton btnApagar = new JButton("Apagar");
		btnApagar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				DeletarCavalo();
				
			}	
		});
		btnApagar.setBounds(273, 116, 98, 46);
		contentPane.add(btnApagar);
		
		txtCod = new JTextField();
		txtCod.setBounds(105, 69, 266, 20);
		contentPane.add(txtCod);
		txtCod.setColumns(10);
		
		JButton btnNewButton = new JButton("Apagar Tudo");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				DeletarCavaloTudo();
				
				
			}
		});
		btnNewButton.setBounds(139, 128, 124, 23);
		contentPane.add(btnNewButton);
	}
	
	private void DeletarCavalo(){
		
		String cod = txtCod.getText();
	
		int codf = Integer.parseInt(cod);
		
		//Posso deletar por chassi e por placa
		if(cod.equals("")){
			return;
		}
		
		controle.DeletarCavalo(codf);
		
			
	}
	
private void DeletarCavaloTudo(){
		
		
		int cod = JOptionPane.showConfirmDialog(null, "Tem certeza?");
		
		switch(cod){
			
			case 0: controle.DeletarTudoCavalo();
			
			case 1: return;
			
		}
		
	
			
	}
	
}
