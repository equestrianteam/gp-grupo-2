package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import DAO.VeterinarioDAO;
import Model.ModeloTabelaVeterinario;

import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JDayChooser;
import com.toedter.calendar.JMonthChooser;
import com.toedter.components.JLocaleChooser;
import com.toedter.calendar.JDateChooser;

import java.awt.Color;

import javax.swing.JScrollBar;
import javax.swing.border.EtchedBorder;

import java.awt.SystemColor;
import java.util.Date;

import javax.swing.border.BevelBorder;
import javax.swing.JTextArea;
import javax.swing.JTable;
import javax.swing.JScrollPane;

public class VeterinarioGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtCavalo;
	private JTextField txtProp;
	private JTextField txtRemedios;
	private JTextField txtValor;
	private JDateChooser Data;
	private JTextArea txtDiagno;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VeterinarioGUI frame = new VeterinarioGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VeterinarioGUI() {
		setTitle("Veterin\u00E1rio");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 462);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblVeterinrio = new JLabel("Veterin\u00E1rio");
		lblVeterinrio.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblVeterinrio.setBounds(138, 11, 186, 14);
		contentPane.add(lblVeterinrio);
		
		JLabel lblNomeDoCavalo = new JLabel("Nome do Cavalo:");
		lblNomeDoCavalo.setBounds(10, 52, 104, 14);
		contentPane.add(lblNomeDoCavalo);
		
		txtCavalo = new JTextField();
		txtCavalo.setBounds(149, 49, 275, 20);
		contentPane.add(txtCavalo);
		txtCavalo.setColumns(10);
		
		JLabel lblProprietrioDoCavalo = new JLabel("Propriet\u00E1rio do Cavalo:");
		lblProprietrioDoCavalo.setBounds(10, 95, 175, 14);
		contentPane.add(lblProprietrioDoCavalo);
		
		txtProp = new JTextField();
		txtProp.setBounds(149, 92, 275, 20);
		contentPane.add(txtProp);
		txtProp.setColumns(10);
		
		JLabel lblRemdios = new JLabel("Rem\u00E9dios:");
		lblRemdios.setBounds(10, 137, 98, 14);
		contentPane.add(lblRemdios);
		
		txtRemedios = new JTextField();
		txtRemedios.setBounds(149, 137, 275, 20);
		contentPane.add(txtRemedios);
		txtRemedios.setColumns(10);
		
		JLabel lblDataDaConsulta = new JLabel("Data da Consulta:");
		lblDataDaConsulta.setBounds(10, 180, 104, 14);
		contentPane.add(lblDataDaConsulta);
		
		JLabel lblValor = new JLabel("Valor:");
		lblValor.setBounds(10, 225, 46, 14);
		contentPane.add(lblValor);
		
		txtValor = new JTextField();
		txtValor.setBounds(149, 222, 275, 20);
		contentPane.add(txtValor);
		txtValor.setColumns(10);
		
		JLabel lblDiagnsticoDoCavalo = new JLabel("Diagn\u00F3stico do Cavalo:");
		lblDiagnsticoDoCavalo.setBounds(10, 263, 145, 14);
		contentPane.add(lblDiagnsticoDoCavalo);
		
		JButton button = new JButton("Fechar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				dispose();
				
			}
		});
		button.setBounds(10, 360, 98, 46);
		contentPane.add(button);
		
		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy/MM-/dd");
				
				String cavalo = txtCavalo.getText();
				String prop = txtProp.getText();
				String remedios = txtRemedios.getText();	
				String consulta = sdf.format(Data.getDate());
				double valor = Double.parseDouble(txtValor.getText());	
				String diagnostico = txtDiagno.getText();	
				
			
				
				ModeloTabelaVeterinario veterinario = new ModeloTabelaVeterinario(cavalo, prop, remedios, consulta, valor, diagnostico);
				
				VeterinarioDAO dao = new VeterinarioDAO();
				
				
				if(dao.Inserir(veterinario)){
					JOptionPane.showMessageDialog(null, "Dados inseridos no banco !");
				} else {
					JOptionPane.showMessageDialog(null, "Erro ao gravar no banco !");
				}
				
				
			}
		});
		btnSalvar.setBounds(326, 360, 98, 46);
		contentPane.add(btnSalvar);
		
		Data = new JDateChooser();
		Data.setBounds(149, 180, 275, 20);
		contentPane.add(Data);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(155, 271, 269, 78);
		contentPane.add(scrollPane);
		
		txtDiagno = new JTextArea();
		scrollPane.setViewportView(txtDiagno);
	}
}
