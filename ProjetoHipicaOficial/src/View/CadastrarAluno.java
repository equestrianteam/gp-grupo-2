package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.xml.crypto.Data;

import DAO.AlunoDAO;
import Model.ModeloTabelaAluno;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Date;

import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JDayChooser;
import com.toedter.calendar.JYearChooser;
import com.toedter.calendar.JMonthChooser;
import java.awt.Toolkit;

public class CadastrarAluno extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtCPF;
	private JTextField txtTel;
	private JTextField txtMail;
	private JTextField txtAula;
	private JTextField txtHora;
	private JDateChooser Data;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastrarAluno frame = new CadastrarAluno();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CadastrarAluno() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(CadastrarAluno.class.getResource("/Imgs/user-128.png")));
		setTitle("Cadastrar Aluno");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 467, 577);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCadastrarCavalo = new JLabel("Cadastrar Aluno");
		lblCadastrarCavalo.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblCadastrarCavalo.setBounds(165, 11, 186, 14);
		contentPane.add(lblCadastrarCavalo);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(10, 53, 46, 14);
		contentPane.add(lblNome);
		
		JLabel lblCpf = new JLabel("CPF:");
		lblCpf.setBounds(10, 96, 29, 14);
		contentPane.add(lblCpf);
		
		JLabel lblNascimento = new JLabel("Nascimento:");
		lblNascimento.setBounds(10, 135, 85, 14);
		contentPane.add(lblNascimento);
		
		JLabel lblTelefone = new JLabel("Telefone:");
		lblTelefone.setBounds(10, 184, 85, 14);
		contentPane.add(lblTelefone);
		
		JLabel lblEmail = new JLabel("E-Mail:");
		lblEmail.setBounds(10, 229, 46, 14);
		contentPane.add(lblEmail);
		
		JLabel lblNewLabel = new JLabel("Aula:");
		lblNewLabel.setBounds(10, 276, 57, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblHorrio = new JLabel("Hor\u00E1rio:");
		lblHorrio.setBounds(10, 316, 67, 14);
		contentPane.add(lblHorrio);
		
		JLabel lblUtilizarEquipamentosEmprestados = new JLabel("Utilizar Equipamentos emprestados?");
		lblUtilizarEquipamentosEmprestados.setBounds(10, 358, 248, 14);
		contentPane.add(lblUtilizarEquipamentosEmprestados);
		
		txtNome = new JTextField();
		txtNome.setBounds(92, 50, 328, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		txtCPF = new JTextField();
		txtCPF.setBounds(92, 93, 328, 20);
		contentPane.add(txtCPF);
		txtCPF.setColumns(10);
		
		txtTel = new JTextField();
		txtTel.setBounds(92, 181, 328, 20);
		contentPane.add(txtTel);
		txtTel.setColumns(10);
		
		txtMail = new JTextField();
		txtMail.setBounds(92, 226, 328, 20);
		contentPane.add(txtMail);
		txtMail.setColumns(10);
		
		txtAula = new JTextField();
		txtAula.setBounds(92, 273, 328, 20);
		contentPane.add(txtAula);
		txtAula.setColumns(10);
		
		txtHora = new JTextField();
		txtHora.setBounds(92, 313, 328, 20);
		contentPane.add(txtHora);
		txtHora.setColumns(10);
		
		JComboBox cbEquip = new JComboBox();
		cbEquip.setModel(new DefaultComboBoxModel(new String[] {"Op\u00E7\u00E3o", " Sim", "N\u00E3o"}));
		cbEquip.setBounds(239, 355, 79, 20);
		contentPane.add(cbEquip);
		
		JLabel lblPossuiCavalo = new JLabel("Possui Cavalo?");
		lblPossuiCavalo.setBounds(10, 402, 130, 14);
		contentPane.add(lblPossuiCavalo);
		
		JComboBox cbCavalo = new JComboBox();
		cbCavalo.setModel(new DefaultComboBoxModel(new String[] {"Op\u00E7\u00E3o", " Sim", "N\u00E3o"}));
		cbCavalo.setBounds(239, 399, 79, 20);
		contentPane.add(cbCavalo);
		
		JComboBox cbPulo = new JComboBox();
		cbPulo.setModel(new DefaultComboBoxModel(new String[] {"Op\u00E7\u00E3o", "40cm", "60cm", "80cm", "90cm", "1m", "1.10m", "1.20m", "1.30m", "1.40m", "1.50m", "1.60m"}));
		cbPulo.setBounds(239, 447, 79, 20);
		contentPane.add(cbPulo);
		
		JLabel lblAlturaDePulo = new JLabel("Altura de Pulo:");
		lblAlturaDePulo.setBounds(10, 450, 130, 14);
		contentPane.add(lblAlturaDePulo);
		
		JButton btnFechar = new JButton("Fechar");
		btnFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				dispose();
				
			}
		});
		btnFechar.setBounds(10, 481, 98, 45);
		contentPane.add(btnFechar);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy/MM/dd");
				
				
				String Nome = txtNome.getText();
				String CPF = txtCPF.getText();
				String nasc = sdf.format(Data.getDate());
				String tel = txtTel.getText();
				String mail = txtMail.getText();
				String aula = txtAula.getText();
				String hora = txtHora.getText();
				

				String equip = cbEquip.getSelectedItem().toString();
				String cavalo = cbCavalo.getSelectedItem().toString();
				String pulo = cbPulo.getSelectedItem().toString();
				

				
				if(equip == "Op��o"){
					JOptionPane.showMessageDialog(null, "Op��o n�o � aceitavel");
					return;
				}
				
				if(cavalo == "Op��o"){
					JOptionPane.showMessageDialog(null, "Op��o n�o � aceitavel");
					return;
				}
				
				if(pulo == "Op��o"){
					JOptionPane.showMessageDialog(null, "Op��o n�o � aceitavel");
					return;
				}
				
				txtNome.setText("");
				txtCPF.setText("");
				txtTel.setText("");
				txtMail.setText("");
				txtAula.setText("");
				txtHora.setText("");
				

				AlunoDAO dao = new AlunoDAO();
				
				ModeloTabelaAluno aluno = new ModeloTabelaAluno(Nome, CPF, nasc, tel, mail, aula, hora, equip, cavalo, pulo);
				
				if(dao.Inserir(aluno)){
					JOptionPane.showMessageDialog(null, "Dados inseridos no banco !");
				} else {
					JOptionPane.showMessageDialog(null, "Erro ao gravar no banco !");
				}
				
			}
		});
		btnCadastrar.setBounds(322, 481, 98, 45);
		contentPane.add(btnCadastrar);
		
		Data = new JDateChooser();
		Data.setBounds(92, 135, 328, 20);
		contentPane.add(Data);
	}
}
