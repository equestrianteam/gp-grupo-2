package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import Controller.ControllerCavalo;
import Model.ModeloTabelaCavalo;

import java.awt.event.ActionEvent;
import java.awt.Toolkit;

public class ListarCavalo extends JFrame {


	private JPanel contentPane;
	private JTextField txtPesquisa;
	ControllerCavalo controle = new ControllerCavalo();
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ListarCavalo frame = new ListarCavalo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ListarCavalo() {
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage(ListarCavalo.class.getResource("/Imgs/listar.jpg")));
		setTitle("Listar Cavalo");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 660, 390);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		txtPesquisa = new JTextField();
		txtPesquisa.setBounds(155, 11, 271, 20);
		contentPane.add(txtPesquisa);
		txtPesquisa.setColumns(10);

		JLabel lblDigiteOCdigo = new JLabel("Digite o c\u00F3digo do cavalo:");
		lblDigiteOCdigo.setBounds(10, 14, 135, 14);
		contentPane.add(lblDigiteOCdigo);

		JButton btnListarTodos = new JButton("Listar Todos");
		btnListarTodos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

					
				listarCavalos();


			}
		});
		btnListarTodos.setBounds(498, 306, 135, 41);
		contentPane.add(btnListarTodos);

		JButton btnPesquisar = new JButton("Pesquisar");
		btnPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				listarPesquisar();

			}
			
		});
		btnPesquisar.setBounds(507, 10, 126, 23);
		contentPane.add(btnPesquisar);

		JButton btnFechara = new JButton("Fechar");
		btnFechara.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				dispose();


			}
		});
		btnFechara.setBounds(10, 306, 135, 41);
		contentPane.add(btnFechara);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 61, 623, 234);
		contentPane.add(scrollPane);

		table = new JTable();
		scrollPane.setViewportView(table);
	}
	
	private void listarCavalos() {
		String codigo = txtPesquisa.getText();
		

		ArrayList<ModeloTabelaCavalo> cavalo = controle.getCavalo();
		if (cavalo != null) {
			DefaultTableModel model = modelTable();
			for (int i = 0; i < cavalo.size(); i++) {
				
				
				
				String nome = cavalo.get(i).getNome();
				int cod = cavalo.get(i).getCod();
				String proprietario = cavalo.get(i).getProprietario();
				String nasc = cavalo.get(i).getNascimento();
				String raca = cavalo.get(i).getRaca();
				String numero = cavalo.get(i).getNumeroBaia();
				String aliment = cavalo.get(i).getAlimentacao();
				String trat = cavalo.get(i).getTratador();
				String pelo = cavalo.get(i).getPelagem();
				


				Object[] row = { nome, cod, proprietario, nasc, raca, numero, aliment, trat, pelo };

				model.addRow(row);
			}
			table.setModel(model);

		} else {
			JOptionPane.showMessageDialog(null, "N�o h� livros para listar");
		}
	}

	//Metodo para incializa��o da formata��o da tabela para pesquisa
	private DefaultTableModel modelTable() {
		DefaultTableModel model = new DefaultTableModel(new Object[][] {

		}, new String[] { "Nome", "Codigo", "Proprietario", "Nascimento", "Raca", "Numero da Baia", "Alimentacao","Tratador", "Pelo"  }) {//campos da tabela
			private static final long serialVersionUID = 1L;
			Class[] types = new Class[] { String.class, int.class, String.class, String.class,String.class, String.class, String.class, String.class, String.class };
			boolean[] canEdit = new boolean[] { false, false, false, false, false, false, false, false, false };

			public Class getColumnClass(int columnIndex) {
				return types[columnIndex];
			}

			public boolean isCellEditable(int rowIndex, int columnIndex) {
				return canEdit[columnIndex];
			}
		};
		return model;
	}
	
	private void listarPesquisar(){
		
		String codigo = txtPesquisa.getText();
		
		if(codigo.equals("")){
			JOptionPane.showMessageDialog(null, "Digite algo para pesquisar");
			return;
		}

		ArrayList<ModeloTabelaCavalo> cavalo = controle.getPesquisar(codigo);
		if (cavalo != null) {
			DefaultTableModel model = modelTable();
			for (int i = 0; i < cavalo.size(); i++) {
				
				
				
				String nome = cavalo.get(i).getNome();
				int cod = cavalo.get(i).getCod();
				String proprietario = cavalo.get(i).getProprietario();
				String nasc = cavalo.get(i).getNascimento();
				String raca = cavalo.get(i).getRaca();
				String numero = cavalo.get(i).getNumeroBaia();
				String aliment = cavalo.get(i).getAlimentacao();
				String trat = cavalo.get(i).getTratador();
				String pelo = cavalo.get(i).getPelagem();
				


				Object[] row = { nome, cod, proprietario, nasc, raca, numero, aliment, trat, pelo };

				model.addRow(row);
			}
			table.setModel(model);

		} else {
			JOptionPane.showMessageDialog(null, "Nada para listar");
		}
	}



	{
	}
}

