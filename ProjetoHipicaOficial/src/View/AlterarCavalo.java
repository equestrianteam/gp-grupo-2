package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.xml.crypto.Data;

import Controller.ControllerCavalo;
import DAO.CavaloDAO;
import Model.ModeloTabelaCavalo;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Toolkit;

import com.toedter.calendar.JDateChooser;

public class AlterarCavalo extends JFrame {
	private JTextField txtProp;
	private JTextField txtNome;
	private JTextField txtPelagem;
	private JTextField txtRaca;
	private JTextField txtNum;
	private JTextField txtAlim;
	private JDateChooser data;
	private JTextField txtTrat;
	private JTextField txtCod;
	private JTextField txtCodAntigo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AlterarCavalo frame = new AlterarCavalo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AlterarCavalo() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(AlterarCavalo.class.getResource("/Imgs/compose-128.png")));
		setTitle("Cadastrar Cavalo");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 468, 567);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCadastrarAluno = new JLabel("Alterar Cavalo");
		lblCadastrarAluno.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblCadastrarAluno.setBounds(145, 11, 194, 14);
		contentPane.add(lblCadastrarAluno);
		
		JLabel lblProprietrio = new JLabel("Propriet\u00E1rio:");
		lblProprietrio.setBounds(17, 55, 78, 14);
		contentPane.add(lblProprietrio);
		
		txtProp = new JTextField();
		txtProp.setBounds(117, 52, 310, 20);
		contentPane.add(txtProp);
		txtProp.setColumns(10);
		
		JLabel lblNome = new JLabel("Nome: ");
		lblNome.setBounds(17, 97, 59, 14);
		contentPane.add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setBounds(117, 94, 310, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblPelagem = new JLabel("Pelagem:");
		lblPelagem.setBounds(17, 141, 59, 14);
		contentPane.add(lblPelagem);
		
		txtPelagem = new JTextField();
		txtPelagem.setBounds(117, 138, 310, 20);
		contentPane.add(txtPelagem);
		txtPelagem.setColumns(10);
		
		JLabel lblNascimento = new JLabel("Nascimento:");
		lblNascimento.setBounds(17, 183, 78, 14);
		contentPane.add(lblNascimento);
		
		JLabel lblRaa = new JLabel("Ra\u00E7a:");
		lblRaa.setBounds(17, 226, 46, 14);
		contentPane.add(lblRaa);
		
		txtRaca = new JTextField();
		txtRaca.setBounds(117, 223, 310, 20);
		contentPane.add(txtRaca);
		txtRaca.setColumns(10);
		
		JLabel lblNmeroDeBaia = new JLabel("N\u00FAmero de Baia:");
		lblNmeroDeBaia.setBounds(15, 271, 125, 14);
		contentPane.add(lblNmeroDeBaia);
		
		txtNum = new JTextField();
		txtNum.setBounds(117, 268, 310, 20);
		contentPane.add(txtNum);
		txtNum.setColumns(10);
		
		JLabel lblAlimentao = new JLabel("Alimenta\u00E7\u00E3o:");
		lblAlimentao.setBounds(16, 312, 104, 14);
		contentPane.add(lblAlimentao);
		
		txtAlim = new JTextField();
		txtAlim.setBounds(117, 309, 310, 20);
		contentPane.add(txtAlim);
		txtAlim.setColumns(10);
		
		JButton btnFechar = new JButton("Fechar");
		btnFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				dispose();
				
			}
		});
		btnFechar.setBounds(17, 471, 98, 46);
		contentPane.add(btnFechar);
		
		JButton button = new JButton("Alterar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				AlterarCavalo();
				
			}
		});
		button.setBounds(165, 471, 98, 46);
		contentPane.add(button);
		
		JButton button_1 = new JButton("Apagar");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				ApagarCavaloGUI a = new ApagarCavaloGUI();
				a.setVisible(true);
				
			}
		});
		button_1.setBounds(332, 471, 98, 46);
		contentPane.add(button_1);
		
		data = new JDateChooser();
		data.setBounds(117, 177, 310, 20);
		contentPane.add(data);
		
		JLabel lblTratador = new JLabel("Tratador:");
		lblTratador.setBounds(17, 348, 78, 14);
		contentPane.add(lblTratador);
		
		txtTrat = new JTextField();
		txtTrat.setBounds(117, 345, 310, 20);
		contentPane.add(txtTrat);
		txtTrat.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("C\u00F3digo Antigo:");
		lblNewLabel.setBounds(17, 386, 103, 14);
		contentPane.add(lblNewLabel);
		
		txtCod = new JTextField();
		txtCod.setBounds(117, 383, 310, 20);
		contentPane.add(txtCod);
		txtCod.setColumns(10);
		
		JLabel lblCdigoNovo = new JLabel("C\u00F3digo novo:");
		lblCdigoNovo.setBounds(17, 424, 97, 14);
		contentPane.add(lblCdigoNovo);
		
		txtCodAntigo = new JTextField();
		txtCodAntigo.setBounds(118, 419, 309, 20);
		contentPane.add(txtCodAntigo);
		txtCodAntigo.setColumns(10);
	}
private void AlterarCavalo(){
		
		ControllerCavalo controle= new ControllerCavalo();
		
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
		
		String Nome = txtNome.getText();
		String prop = txtProp.getText();		
		String nasc = sdf.format(data.getDate());
		String raca = txtRaca.getText();
		String num = txtNum.getText();
		String alim = txtAlim.getText();
		String trat = txtTrat.getText();
		String pelo = txtPelagem.getText();
		int cod = Integer.parseInt(txtCod.getText());
		int codA= Integer.parseInt(txtCodAntigo.getText());
				
		controle.Alterar(prop, Nome, pelo, nasc, raca, num, alim, trat,cod);
	}


}
