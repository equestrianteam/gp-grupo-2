package DAO;


	import Controller.FabricaDeConexao;
	import java.sql.Connection;
	import java.sql.PreparedStatement;
	import java.sql.ResultSet;
	import java.sql.SQLException;
	import java.util.ArrayList;
	import javax.swing.JOptionPane;
	import Model.ModeloMySql;
	import Model.ModeloTabelaCavalo;
	
	public class CavaloDAO {
		private ModeloMySql modelo = null;
		public CavaloDAO(){
			modelo = new ModeloMySql();
			/* o que eu preciso para conectar no banco */
			modelo.setDatabase("hipicaOficial");
			modelo.setServer("localhost");
			modelo.setUser("root");
			modelo.setPassword("ifsuldeminas");
			modelo.setPort("3306");
		}
		public boolean Inserir(ModeloTabelaCavalo cavalo){
			boolean status = true;
			Connection con = FabricaDeConexao.getConnection(modelo);
			/* testa se conseguiu conectar */
			if(con == null){
				JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
				return false;
			}
			/* prepara o sql para inserir no banco */
			String sql = "INSERT INTO cavalo (nome,prop,nasc,raca,num,alim,trat,pelo,cod) VALUES(?,?,?,?,?,?,?,?,?);";
			/* prepara para inserir no banco de dados */
			try {
				PreparedStatement statement = con.prepareStatement(sql);
				/* preenche com os dados */
				
				statement.setString(1, cavalo.getNome());
				statement.setString(2, cavalo.getProprietario());
				statement.setString(3, cavalo.getNascimento());
				statement.setString(4, cavalo.getRaca());
				statement.setString(5, cavalo.getNumeroBaia());
				statement.setString(6, cavalo.getAlimentacao());
				statement.setString(7, cavalo.getTratador());
				statement.setString(8, cavalo.getPelagem());
				statement.setInt(9, cavalo.getCod());
				
				/* executa o comando no banco */
				statement.executeUpdate();
				status = true;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				status = false;
			} finally {
				// Fecha a conexao 
				FabricaDeConexao.closeConnection(con);
			}
			return status;
		}
		

		public boolean Alterar(ModeloTabelaCavalo cavalo, int codA){
			boolean status = true;
			Connection con = FabricaDeConexao.getConnection(modelo);
			/* testa se conseguiu conectar */
			if(con == null){
				JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
				return false;
			}
			String sql = " UPDATE cavalo SET Nome=?, prop=?, nasc=?, raca=?, num=?, alim=?, trat=?, pelo=? WHERE cod=?;";
		//	create table cavalo (Nome varchar(50), prop varchar(50), nasc date, raca varchar(40), num int, alim varchar(70), trat varchar(40), pelo varchar(40), cod int, primary key(cod)) engine  = innodb;
			
			try {
				PreparedStatement statement = con.prepareStatement(sql);
				/* preenche com os dados */
				statement.setString(1, cavalo.getNome());
				statement.setString(2, cavalo.getProprietario());
				statement.setString(3, cavalo.getNascimento());
				statement.setString(4, cavalo.getRaca());
				statement.setString(5, cavalo.getNumeroBaia());
				statement.setString(6, cavalo.getAlimentacao());
				statement.setString(7, cavalo.getTratador());
				statement.setString(8, cavalo.getPelagem());
				statement.setInt(9, codA);
				System.out.println(statement);
				/* executa o comando no banco */
				statement.executeUpdate();
				status = true;
			} catch (SQLException e) {
				status = false;
			} finally {
				// Fecha a conexao 
				FabricaDeConexao.closeConnection(con);
			}
			return true;
		}
		
		
		
		
		
		public boolean Excluir(int key){
			boolean status = true;
			Connection con = FabricaDeConexao.getConnection(modelo);
			/* testa se conseguiu conectar */
			if(con == null){
				JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
				return false;
			}
			String sql = " DELETE from cavalo WHERE cod=?;";
			try {
				PreparedStatement statement = con.prepareStatement(sql);
				/* preenche com os dados */
				statement.setInt(1, key);
				/* executa o comando no banco */
				statement.executeUpdate();
				status = true;
			} catch (SQLException e) {
				status = false;
			} finally {
				// Fecha a conexao 
				FabricaDeConexao.closeConnection(con);
			}
			return true;
		}
		
		public boolean ExcluirTudo(){
			boolean status = true;
			Connection con = FabricaDeConexao.getConnection(modelo);
			/* testa se conseguiu conectar */
			if(con == null){
				JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
				return false;
			}
			String sql = " DELETE from cavalo;";
			try {
				PreparedStatement statement = con.prepareStatement(sql);
				/* preenche com os dados */
				
				/* executa o comando no banco */
				statement.executeUpdate();
				status = true;
			} catch (SQLException e) {
				status = false;
			} finally {
				// Fecha a conexao 
				FabricaDeConexao.closeConnection(con);
			}
			return true;
		}
		
		public ArrayList<ModeloTabelaCavalo> Listar(){
			
			Connection con = FabricaDeConexao.getConnection(modelo);

			/* vetor para armazenar os livros obtidos */
			ArrayList<ModeloTabelaCavalo> cavalo = new ArrayList<ModeloTabelaCavalo>();
			
			/* testa se conseguiu conectar */
			if(con == null){
				JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
				return null;
			}
			
			
			/* prepara o sql para selecionar no banco */
			
			String sql = "SELECT * FROM cavalo;";
			
			/* prepara para selecionar no banco de dados */
			
			try {
				PreparedStatement statement = con.prepareStatement(sql);
				
				ResultSet result = statement.executeQuery();
				
				/* recebe os resultados da query */
				while(result.next()){
					ModeloTabelaCavalo cavalos = new ModeloTabelaCavalo();
					cavalos.setNome(result.getString("Nome"));
					cavalos.setProprietario(result.getString("prop"));
					cavalos.setNascimento(result.getString("nasc"));
					cavalos.setRaca(result.getString("raca"));
					cavalos.setNumeroBaia(result.getString("num"));
					cavalos.setAlimentacao(result.getString("alim"));
					cavalos.setTratador(result.getString("trat"));
					cavalos.setPelagem(result.getString("pelo"));
					cavalos.setCod(result.getInt("cod"));
					
					/*adiciona novo livro encontrado no vetor de livros*/
					cavalo.add(cavalos);
				}
						
							
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				// Fecha a conexao 
				FabricaDeConexao.closeConnection(con);
			
			}
			
			return cavalo;
		}
		
public ArrayList<ModeloTabelaCavalo> Pesquisar(String key){
			
			Connection con = FabricaDeConexao.getConnection(modelo);

			/* vetor para armazenar os livros obtidos */
			ArrayList<ModeloTabelaCavalo> cavalo = new ArrayList<ModeloTabelaCavalo>();
			
			/* testa se conseguiu conectar */
			if(con == null){
				JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
				return null;
			}
			
			
			/* prepara o sql para selecionar no banco */
			
			String sql = "SELECT * FROM cavalo where cod = '" +key+ "';";
			
			/* prepara para selecionar no banco de dados */
			
			try {
				PreparedStatement statement = con.prepareStatement(sql);
				
				ResultSet result = statement.executeQuery();
				
				/* recebe os resultados da query */
				while(result.next()){
					ModeloTabelaCavalo cavalos = new ModeloTabelaCavalo();
					cavalos.setNome(result.getString("Nome"));
					cavalos.setProprietario(result.getString("prop"));
					cavalos.setNascimento(result.getString("nasc"));
					cavalos.setRaca(result.getString("raca"));
					cavalos.setNumeroBaia(result.getString("num"));
					cavalos.setAlimentacao(result.getString("alim"));
					cavalos.setTratador(result.getString("trat"));
					cavalos.setPelagem(result.getString("pelo"));
					cavalos.setCod(result.getInt("cod"));
					
					/*adiciona novo livro encontrado no vetor de livros*/
					cavalo.add(cavalos);
				}
						
							
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				// Fecha a conexao 
				FabricaDeConexao.closeConnection(con);
			
			}
			
			return cavalo;
		}
		
		
              
	}