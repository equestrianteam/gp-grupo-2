package DAO;

import Controller.FabricaDeConexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import Model.ModeloMySql;
import Model.ModeloTabelaAluno;
import Model.ModeloTabelaCavalo;


public class AlunoDAO {
	private ModeloMySql modelo = null;

	/**
	 * @wbp.parser.entryPoint
	 */
	public AlunoDAO(){

		modelo = new ModeloMySql();

		/* o que eu preciso para conectar no banco */
		modelo.setDatabase("hipicaOficial");
		modelo.setServer("localhost");
		modelo.setUser("root");
		modelo.setPassword("ifsuldeminas");
		modelo.setPort("3306");
	}


	public boolean Inserir(ModeloTabelaAluno aluno){


		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
			return false;
		}


		/* prepara o sql para inserir no banco */

		String sql = "INSERT INTO aluno (Nome, CPF, nasc, tel, mail, aula, hora, equip, cavalo, pulo) VALUES(?,?,?,?,?,?,?,?,?,?);";

		/* prepara para inserir no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, aluno.getNome());
			statement.setString(2, aluno.getCpf());
			statement.setString(3, aluno.getNascimento());
			statement.setString(4, aluno.getTelefone());
			statement.setString(5, aluno.getEmail());
			statement.setString(6, aluno.getAulas());
			statement.setString(7, aluno.getHorarios());
			statement.setString(8, aluno.getEquipamentos());
			statement.setString(9, aluno.getPossuiCavalo());
			statement.setString(10, aluno.getPulo());
			

			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return status;

	}
	public boolean Alterar(ModeloTabelaAluno aluno, String key){

		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
			return false;
		}

		String sql = " UPDATE aluno SET Nome=?, CPF=?, nasc=?, tel=?, mail=?, aula=?, hora=?, equip=?, cavalo=?, pulo=? WHERE CPF=?;";


		
		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, aluno.getNome());
			statement.setString(2, aluno.getCpf());
			statement.setString(3, aluno.getNascimento());
			statement.setString(4, aluno.getTelefone());
			statement.setString(5, aluno.getEmail());
			statement.setString(6, aluno.getHorarios());
			statement.setString(7, aluno.getEquipamentos());
			statement.setString(8, aluno.getPossuiCavalo());
			statement.setString(9, aluno.getPulo());
			statement.setString(10, key);


			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return true;
	}
	public boolean Excluir(String key){

		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
			return false;
		}

		String sql = "DELETE from aluno WHERE CPF=?;";

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, key);


			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return true;
	}
	public ArrayList<ModeloTabelaAluno> Listar(){

		Connection con = FabricaDeConexao.getConnection(modelo);

		/* vetor para armazenar os livros obtidos */
		ArrayList<ModeloTabelaAluno> alunos = new ArrayList<ModeloTabelaAluno>();

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
			return null;
		}


		/* prepara o sql para selecionar no banco */

		String sql = "SELECT * FROM aluno;";

		/* prepara para selecionar no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			ResultSet result = statement.executeQuery();

			/* recebe os resultados da query */
			while(result.next()){
				ModeloTabelaAluno aluno = new ModeloTabelaAluno();
				aluno.setNome(result.getString("Nome"));
				aluno.setCpf(result.getString("CPF"));
				aluno.setNascimento(result.getString("nasc"));
				aluno.setTelefone(result.getString("tel"));
				aluno.setEmail(result.getString("mail"));
				aluno.setHorarios(result.getString("hora"));
				aluno.setEquipamentos(result.getString("equip"));
				aluno.setPossuiCavalo(result.getString("cavalo"));
				aluno.setPulo(result.getString("pulo"));
				
				
				

				/*adiciona novo livro encontrado no vetor de livros*/
				alunos.add(aluno);
			}


		} catch (SQLException e) {
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return alunos;
	}      
	
	public ArrayList<ModeloTabelaAluno> Pesquisar(String key){

		Connection con = FabricaDeConexao.getConnection(modelo);

		/* vetor para armazenar os livros obtidos */
		ArrayList<ModeloTabelaAluno> alunos = new ArrayList<ModeloTabelaAluno>();

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
			return null;
		}


		/* prepara o sql para selecionar no banco */

		String sql = "SELECT * FROM aluno where CPF = '" +key+ "';";

		/* prepara para selecionar no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			ResultSet result = statement.executeQuery();
			
			

			/* recebe os resultados da query */
			while(result.next()){
				ModeloTabelaAluno aluno = new ModeloTabelaAluno();
				aluno.setNome(result.getString("Nome"));
				aluno.setCpf(result.getString("CPF"));
				aluno.setNascimento(result.getString("nasc"));
				aluno.setTelefone(result.getString("tel"));
				aluno.setEmail(result.getString("mail"));
				aluno.setHorarios(result.getString("hora"));
				aluno.setEquipamentos(result.getString("equip"));
				aluno.setPossuiCavalo(result.getString("cavalo"));
				aluno.setPulo(result.getString("pulo"));
				
				
				

				/*adiciona novo livro encontrado no vetor de livros*/
				alunos.add(aluno);
			}


		} catch (SQLException e) {
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return alunos;
	}      
	
	public boolean ExcluirTudo(){

		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
			return false;
		}

		String sql = "DELETE from aluno ;";

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			
			


			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return true;
	}


}

