package DAO;

	import Controller.FabricaDeConexao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import Model.ModeloMySql;
import Model.ModeloTabelaFuncionario;
import Model.ModeloTabelaFuncionario;
	
	public class FuncionarioDAO {
		
	
		private ModeloMySql modelo = null;
		public FuncionarioDAO(){
			modelo = new ModeloMySql();
			modelo.setDatabase("hipicaOficial");
			modelo.setServer("localhost");
			modelo.setUser("root");
			modelo.setPassword("ifsuldeminas");
			modelo.setPort("3306");
		}
		
		
		public boolean Inserir(ModeloTabelaFuncionario Funcionario){
			boolean status = true;
			Connection con = FabricaDeConexao.getConnection(modelo);

			if(con == null){
				JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
				return false;
			}
			
			
			
			/* prepara o sql para inserir no banco */
			String sql = "INSERT INTO funcionario (nome,cpf,nasc,tel,mail,dpt,dias,hora) VALUES(?,?,?,?,?,?,?,?);";
			/* prepara para inserir no banco de dados */
			
			try {
				PreparedStatement statement = con.prepareStatement(sql);
				/* preenche com os dados */
				statement.setString(1, Funcionario.getNome());
				statement.setString(2, Funcionario.getCpf());
				statement.setString(3, Funcionario.getNascimento());
				statement.setString(4, Funcionario.getTelefone());
				statement.setString(5, Funcionario.getEmail());
				statement.setString(6, Funcionario.getDiasDeTrabalho());
				statement.setString(7, Funcionario.getHorario());
				statement.setString(8, Funcionario.getDepartamento());
				/* executa o comando no banco */
				statement.executeUpdate();
				status = true;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				status = false;
			} finally {
				// Fecha a conexao 
				FabricaDeConexao.closeConnection(con);
			}
			return status;
		}
		
		public boolean Alterar(ModeloTabelaFuncionario Funcionario, String key){
			boolean status = true;
			Connection con = FabricaDeConexao.getConnection(modelo);
			/* testa se conseguiu conectar */
			if(con == null){
				JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
				return false;
			}
			String sql = " UPDATE Funcionario SET prop=?, nome=?, pelagem=?, nasc=?, raca=?, num=?, alim=?, trat=? WHERE nome=?;";
			try {
				PreparedStatement statement = con.prepareStatement(sql);
				/* preenche com os dados */
				statement.setString(1, Funcionario.getNome());
				statement.setString(2, Funcionario.getCpf());
				statement.setString(3, Funcionario.getNascimento());
				statement.setString(4, Funcionario.getTelefone());
				statement.setString(5, Funcionario.getEmail());
				statement.setString(6, Funcionario.getDiasDeTrabalho());
				statement.setString(7, Funcionario.getHorario());
				statement.setString(8, Funcionario.getDepartamento());
				statement.setString(9, key);
				/* executa o comando no banco */
				statement.executeUpdate();
				status = true;
			} catch (SQLException e) {
				status = false;
			} finally {
				// Fecha a conexao 
				FabricaDeConexao.closeConnection(con);
			}
			return true;
		}
		
		public boolean Excluir(String key){
			boolean status = true;
			Connection con = FabricaDeConexao.getConnection(modelo);
			/* testa se conseguiu conectar */
			if(con == null){
				JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
				return false;
			}
			String sql = " DELETE from Funcionario WHERE nome=?;";
			try {
				PreparedStatement statement = con.prepareStatement(sql);
				/* preenche com os dados */
				statement.setString(1, key);
				/* executa o comando no banco */
				statement.executeUpdate();
				status = true;
			} catch (SQLException e) {
				status = false;
			} finally {
				// Fecha a conexao 
				FabricaDeConexao.closeConnection(con);
			}
			return true;
		}
		
		public ArrayList<ModeloTabelaFuncionario> Listar(){
			Connection con = FabricaDeConexao.getConnection(modelo);
			/* vetor para armazenar os livros obtidos */
			ArrayList<ModeloTabelaFuncionario> Funcionarios = new ArrayList<ModeloTabelaFuncionario>();
			/* testa se conseguiu conectar */
			if(con == null){
				JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
				return null;
			}
			/* prepara o sql para selecionar no banco */
			String sql = "SELECT * FROM Funcionario;";
			/* prepara para selecionar no banco de dados */
			try {
				PreparedStatement statement = con.prepareStatement(sql);
				ResultSet result = statement.executeQuery();
				/* recebe os resultados da query */
				while(result.next()){
					ModeloTabelaFuncionario Funcionario = new ModeloTabelaFuncionario();
					Funcionario.setNome(result.getString("Nome"));
					Funcionario.setCpf(result.getString("cpf"));
					Funcionario.setNascimento(result.getString("nasc"));
					Funcionario.setTelefone(result.getString("tel"));
					Funcionario.setEmail(result.getString("mail"));
					Funcionario.setDepartamento(result.getString("dpt"));
					Funcionario.setDiasDeTrabalho(result.getString("dias"));
					Funcionario.setHorario(result.getString("hora"));
					/*adiciona novo livro encontrado no vetor de livros*/

					
					//create table funcionario(Nome varchar(50), cpf varchar(50), nasc date, tel varchar(30), mail varchar(60), dpt varchar(40), dias varchar(50),
						//	hora varchar(100), primary key(cpf)) engine = innodb;

					
					Funcionarios.add(Funcionario);
				}
			} catch (SQLException e) {
			} finally {
				// Fecha a conexao 
				FabricaDeConexao.closeConnection(con);
			}
			return Funcionarios;
		}                
	}
