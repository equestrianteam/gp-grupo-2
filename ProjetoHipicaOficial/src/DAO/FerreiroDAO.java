package DAO;

import Controller.FabricaDeConexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import Model.ModeloMySql;
import Model.ModeloTabelaFerreiro;
import Model.ModeloTabelaFerreiro;

public class FerreiroDAO {
	
	private ModeloMySql modelo = null;
	public FerreiroDAO(){
		
		modelo = new ModeloMySql();
		modelo.setDatabase("hipicaOficial");
		modelo.setServer("localhost");
		modelo.setUser("root");
		modelo.setPassword("ifsuldeminas");
		modelo.setPort("3306");
	}
	public boolean Inserir(ModeloTabelaFerreiro ferreiro){
		boolean status = true;
		
		Connection con = FabricaDeConexao.getConnection(modelo);
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
			return false;
		}
		
	
		
		
		
		/* prepara o sql para inserir no banco */
		String sql = "INSERT INTO ferreiro (cavalo, prop, consulta, valor, tamanho) VALUES(?,?,?,?,?);";
		/* prepara para inserir no banco de dados */
		try {
			PreparedStatement statement = con.prepareStatement(sql);
			/* preenche com os dados */
			statement.setString(1, ferreiro.getNome());
			statement.setString(2, ferreiro.getProprietario());
			statement.setString(3, ferreiro.getDataDeConsulta());
			statement.setDouble(4, ferreiro.getValor());
			statement.setDouble(5, ferreiro.getTamanhoFerradura());
			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);
		}
		return status;
	}
}