package DAO;

import Controller.FabricaDeConexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import Model.ModeloMySql;
import Model.ModeloTabelaVeterinario;
import Model.ModeloTabelaVeterinario;

public class VeterinarioDAO {
	
	private ModeloMySql modelo = null;
	public VeterinarioDAO(){
		modelo = new ModeloMySql();
		/* o que eu preciso para conectar no banco */
		modelo.setDatabase("hipicaOficial");
		modelo.setServer("localhost");
		modelo.setUser("root");
		modelo.setPassword("ifsuldeminas");
		modelo.setPort("3306");
	}
	
	public boolean Inserir(ModeloTabelaVeterinario veterinario){
		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);
		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
			return false;
		}
		
		
		
		String sql = "INSERT INTO veterinario (nome, prop, remedios, dataConsulta, valor, diagnostico) VALUES(?,?,?,?,?,?);";
		
		
		try {
			PreparedStatement statement = con.prepareStatement(sql);
			
			/* preenche com os dados */
			statement.setString(1, veterinario.getNome());
			statement.setString(2, veterinario.getProprietario());
			statement.setString(3, veterinario.getRemedios());
			statement.setString(4, veterinario.getDataConsulta());
			statement.setDouble(5, veterinario.getValor());
			statement.setString(6, veterinario.getDiagnostico());
			
			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);
		}
		return status;
	}
}
	
	
