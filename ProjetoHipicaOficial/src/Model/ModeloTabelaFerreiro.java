/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;


public class ModeloTabelaFerreiro {
    
    private String nome;
    private String proprietario;
    private String dataDeConsulta;
    private double valor;
    private double tamanhoFerradura;

    public ModeloTabelaFerreiro() {
		super();
	}
		

   
    public ModeloTabelaFerreiro(String nome, String proprietario,
			String dataDeConsulta, double valor, double tamanhoFerradura) {
		super();
		this.nome = nome;
		this.proprietario = proprietario;
		this.dataDeConsulta = dataDeConsulta;
		this.valor = valor;
		this.tamanhoFerradura = tamanhoFerradura;
	}


	public String getNome() {
        return nome;
    }

   
    public void setNome(String nome) {
        this.nome = nome;
    }

    
    public String getProprietario() {
        return proprietario;
    }

   
    public void setProprietario(String proprietario) {
        this.proprietario = proprietario;
    }

   
    public String getDataDeConsulta() {
        return dataDeConsulta;
    }

   
    public void setDataDeConsulta(String dataDeConsulta) {
        this.dataDeConsulta = dataDeConsulta;
    }

   
    public double getValor() {
        return valor;
    }

   
    public void setValor(double valor) {
        this.valor = valor;
    }

    
    public double getTamanhoFerradura() {
        return tamanhoFerradura;
    }

    
    public void setTamanhoFerradura(double tamanhoFerradura) {
        this.tamanhoFerradura = tamanhoFerradura;
    }
        
        
        
}
    
    
   