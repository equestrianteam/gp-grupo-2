package Model;

public class ModeloTabelaCavalo {
    private String proprietario;
    private String nome;
    private String pelagem;
    private String nascimento;
    private String raca;
    private String numeroBaia;
    private String alimentacao;
    private String tratador;
    private int cod;
    
   
    public ModeloTabelaCavalo() {
		super();
	}
    
	public ModeloTabelaCavalo(String proprietario, String nome, String pelagem,
			String nascimento, String raca, String numeroBaia,
			String alimentacao, String tratador, int cod) {
		super();
		this.proprietario = proprietario;
		this.nome = nome;
		this.pelagem = pelagem;
		this.nascimento = nascimento;
		this.raca = raca;
		this.numeroBaia = numeroBaia;
		this.alimentacao = alimentacao;
		this.tratador = tratador;
		this.cod = cod;
	}


	public String getProprietario() {
		return proprietario;
	}


	public void setProprietario(String proprietario) {
		this.proprietario = proprietario;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getPelagem() {
		return pelagem;
	}


	public void setPelagem(String pelagem) {
		this.pelagem = pelagem;
	}


	public String getNascimento() {
		return nascimento;
	}


	public void setNascimento(String nascimento) {
		this.nascimento = nascimento;
	}


	public String getRaca() {
		return raca;
	}


	public void setRaca(String raca) {
		this.raca = raca;
	}


	public String getNumeroBaia() {
		return numeroBaia;
	}


	public void setNumeroBaia(String numeroBaia) {
		this.numeroBaia = numeroBaia;
	}


	public String getAlimentacao() {
		return alimentacao;
	}


	public void setAlimentacao(String alimentacao) {
		this.alimentacao = alimentacao;
	}


	public String getTratador() {
		return tratador;
	}


	public void setTratador(String tratador) {
		this.tratador = tratador;
	}


	public int getCod() {
		return cod;
	}


	public void setCod(int cod) {
		this.cod = cod;
	}

	

}