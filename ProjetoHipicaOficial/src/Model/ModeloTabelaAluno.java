/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;


public class ModeloTabelaAluno {
    private String nome;
    private String cpf;
    private String nascimento;
    private String telefone;
    private String email;
    private String aulas;
    private String horarios;
    private String equipamentos;
    private String possuiCavalo;
    private String pulo;
    
    public ModeloTabelaAluno(){}
    
    public ModeloTabelaAluno(String nome,String cpf,String nascimento,String telefone,String email,String aulas,String horarios,String equipamentos,String possuiCavalo, String pulo){
        this.nome= nome;
        this.cpf=cpf;
        this.nascimento=nascimento;
        this.telefone=telefone;
        this.email=email;
        this.aulas=aulas;
        this.horarios=horarios;
        this.equipamentos=equipamentos;
        this.possuiCavalo=possuiCavalo;
        this.pulo=pulo;
        
    }

   
	public String getNome() {
        return nome;
    }

   
    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

   
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

   
    public String getNascimento() {
        return nascimento;
    }

   
    public void setNascimento(String nascimento) {
        this.nascimento = nascimento;
    }

   
    public String getTelefone() {
        return telefone;
    }

  
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

   
    public String getEmail() {
        return email;
    }

   
    public void setEmail(String email) {
        this.email = email;
    }

    
    public String getAulas() {
        return aulas;
    }

    
    public void setAulas(String aulas) {
        this.aulas = aulas;
    }

   
    public String getHorarios() {
        return horarios;
    }

   
    public void setHorarios(String horarios) {
        this.horarios = horarios;
    }

   
    public String getEquipamentos() {
        return equipamentos;
    }

    
    public void setEquipamentos(String equipamentos) {
        this.equipamentos = equipamentos;
    }

   
    public String getPossuiCavalo() {
        return possuiCavalo;
    }

    
    public void setPossuiCavalo(String possuiCavalo) {
        this.possuiCavalo = possuiCavalo;
    }
    

    public String getPulo() {
		return pulo;
	}

	public void setPulo(String pulo) {
		this.pulo = pulo;
	}

}

