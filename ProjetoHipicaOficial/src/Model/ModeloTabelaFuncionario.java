package Model;


public class ModeloTabelaFuncionario {
    private String nome;
    private String cpf;
    private String nascimento;
    private String telefone;
    private String email;
    private String diasDeTrabalho;
    private String horario;
    private String departamento;
    
    public ModeloTabelaFuncionario(){}
    
    public ModeloTabelaFuncionario(String nome,String cpf,String nascimento,String telefone,String email,String diasDeTrabalho,String horario,String departamento){
        this.nome= nome;
        this.cpf=cpf;
        this.nascimento=nascimento;
        this.telefone=telefone;
        this.email=email;
        this.diasDeTrabalho=diasDeTrabalho;
        this.horario=horario;
        this.departamento=departamento;
    }

   
    public String getNome() {
        return nome;
    }

   
    public void setNome(String nome) {
        this.nome = nome;
    }

   
    public String getCpf() {
        return cpf;
    }

    
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    
    public String getNascimento() {
        return nascimento;
    }

    
    public void setNascimento(String nascimento) {
        this.nascimento = nascimento;
    }

    
    public String getTelefone() {
        return telefone;
    }

    
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    
    public String getEmail() {
        return email;
    }

   
    public void setEmail(String email) {
        this.email = email;
    }

    
    public String getDiasDeTrabalho() {
        return diasDeTrabalho;
    }

   
    public void setDiasDeTrabalho(String diasDeTrabalho) {
        this.diasDeTrabalho = diasDeTrabalho;
    }

    
    public String getHorario() {
        return horario;
    }

    
    public void setHorario(String horario) {
        this.horario = horario;
    }

   
    public String getDepartamento() {
        return departamento;
    }

    
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }
}
