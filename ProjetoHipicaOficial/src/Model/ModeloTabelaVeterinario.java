/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;


public class ModeloTabelaVeterinario {
    
    private String nome;
    private String proprietario;
    private String remedios;
    private String dataConsulta;
    private double valor;
    private String diagnostico;

    public ModeloTabelaVeterinario() {
		super();
	}
	
	public ModeloTabelaVeterinario(String nome, String proprietario, 
                String remedios, String dataConsulta, double valor,String diagnostico){
		super();
		this.nome = nome;
		this.proprietario = proprietario;
		this.remedios = remedios;
		this.dataConsulta = dataConsulta;
		this.valor = valor;
		this.diagnostico = diagnostico;
	}
    
    
    public String getNome() {
        return nome;
    }

    
    public void setNome(String nome) {
        this.nome = nome;
    }

    
    public String getProprietario() {
        return proprietario;
    }

    
    public void setProprietario(String proprietario) {
        this.proprietario = proprietario;
    }

   
    public String getRemedios() {
        return remedios;
    }

    
    public void setRemedios(String remedios) {
        this.remedios = remedios;
    }

    
    public String getDataConsulta() {
        return dataConsulta;
    }

   
    public void setDataConsulta(String dataConsulta) {
        this.dataConsulta = dataConsulta;
    }

    
    public double getValor() {
        return valor;
    }

    
    public void setValor(double valor) {
        this.valor = valor;
    }

    
    public String getDiagnostico() {
        return diagnostico;
    }

    
    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }
    
    
    
    
    
}
