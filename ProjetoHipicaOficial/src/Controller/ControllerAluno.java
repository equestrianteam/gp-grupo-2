package Controller;

import java.util.ArrayList;
import java.util.Random;

import javax.swing.JOptionPane;

import DAO.AlunoDAO;
import DAO.CavaloDAO;
import Model.ModeloTabelaAluno;
import Model.ModeloTabelaCavalo;

public class ControllerAluno {
	
	
	public ArrayList<ModeloTabelaAluno> getAluno() {
		ArrayList<ModeloTabelaAluno> aluno = null;
		AlunoDAO daoAluno = new AlunoDAO();
		aluno = daoAluno.Listar();
		return aluno;
		
	}
	
	public ArrayList<ModeloTabelaAluno> getPesquisar(String key) {
		ArrayList<ModeloTabelaAluno> aluno = null;
		AlunoDAO daoAluno = new AlunoDAO();
		aluno = daoAluno.Pesquisar(key);
		return aluno;
	}
	
	
	public void Alterar (String nome,String cpf,String nascimento,String telefone,String email,String aulas,String horarios,String equipamentos,String possuiCavalo, String pulo){ 
		

		
		
		ModeloTabelaAluno aluno = new ModeloTabelaAluno(nome, cpf, nascimento, telefone, email, aulas, horarios, equipamentos, possuiCavalo, pulo);
		AlunoDAO dao = new AlunoDAO();
		
		if(dao.Alterar(aluno, cpf)){
			JOptionPane.showMessageDialog(null, "Sucesso");		
		}else{
			JOptionPane.showMessageDialog(null, "Erro");		
			}
	}

	public void Inserir(String Nome, String CPF, String nasc, String tel, String mail, String aula, String hora, String equip, String cavalo, String pulo) {
		

	
		
		ModeloTabelaAluno aluno = new ModeloTabelaAluno(Nome, CPF, nasc, tel, mail, aula, hora, equip, cavalo, pulo);
		AlunoDAO daoAluno = new AlunoDAO();
		if(daoAluno.Inserir(aluno)){
			JOptionPane.showMessageDialog(null, "Gravado com sucesso");
		}else{
			JOptionPane.showMessageDialog(null, "Erro");
		}
	}
	
	
		public void DeletarAluno(String cpf) {
			
			
			AlunoDAO dao = new AlunoDAO();
			if(dao.Excluir(cpf )){
				JOptionPane.showMessageDialog(null, "Sucesso");
			}else{
				JOptionPane.showMessageDialog(null, "Erro");
			}
			
		}
		
		public void DeletarTudoAluno() {
			
			
			AlunoDAO dao = new AlunoDAO();
			if(dao.ExcluirTudo()){
				JOptionPane.showMessageDialog(null, "Tudo apagado com sucesso");
			}else{
				JOptionPane.showMessageDialog(null, "Erro");
			}
			
		}	
	
}
