package Controller;

import java.util.ArrayList;
import java.util.Random;

import javax.swing.JOptionPane;

import DAO.AlunoDAO;
import DAO.CavaloDAO;
import Model.ModeloTabelaAluno;
import Model.ModeloTabelaCavalo;

public class ControllerCavalo {
	
	public ArrayList<ModeloTabelaCavalo> getCavalo() {
		ArrayList<ModeloTabelaCavalo> cavalo = null;
		CavaloDAO daoCavalo = new CavaloDAO();
		cavalo = daoCavalo.Listar();
		return cavalo;
	}
	
	public ArrayList<ModeloTabelaCavalo> getPesquisar(String key) {
		ArrayList<ModeloTabelaCavalo> cavalo = null;
		CavaloDAO daoCavalo = new CavaloDAO();
		cavalo = daoCavalo.Pesquisar(key);
		return cavalo;
	
	}
	
	
	
	public void Inserir(String Nome, String prop, String nasc, String raca, String num, String alim,String trat, String pelo, int cod) {
		

		
		ModeloTabelaCavalo cavalo = new ModeloTabelaCavalo(prop, Nome, pelo, nasc, raca, num, alim, trat, cod);
		CavaloDAO dao = new CavaloDAO();
		
		if(dao.Inserir(cavalo)){
			JOptionPane.showMessageDialog(null, "Gravado com sucesso");
		}else{
			JOptionPane.showMessageDialog(null, "Erro");
		}
	}
	
	public void Alterar(String Nome, String prop, String nasc, String raca, String num, String alim,String trat, String pelo, int cod) {
		
	
		ModeloTabelaCavalo cavalo = new ModeloTabelaCavalo(prop, Nome, pelo, nasc, raca, num, alim, trat, cod);
		CavaloDAO dao = new CavaloDAO();
		
		if(dao.Alterar(cavalo, cod)){
			JOptionPane.showMessageDialog(null, "Sucesso");		
		}else{
			JOptionPane.showMessageDialog(null, "Erro");		
			}
	}
	
	
	
	public void DeletarCavalo(int cod) {
			
			
			CavaloDAO dao = new CavaloDAO();
			if(dao.Excluir(cod )){
				JOptionPane.showMessageDialog(null, "Sucesso");
			}else{
				JOptionPane.showMessageDialog(null, "Erro");
			}
			
		}	
	
	public void DeletarTudoCavalo() {
		
		
		CavaloDAO dao = new CavaloDAO();
		if(dao.ExcluirTudo()){
			JOptionPane.showMessageDialog(null, "Tudo apagado com sucesso");
		}else{
			JOptionPane.showMessageDialog(null, "Erro");
		}
		
	}	
	


}